import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';

interface Post {
  title: string;
  message: string;
}

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent implements OnInit {

  form: FormGroup;

  posts: Post[] = [
    {
      title: 'Titre',
      message: 'Example'
    }
  ];

  constructor(fb: FormBuilder) {
    this.form = fb.group({
      title: ['', [Validators.required]],
      message: ['']
    });
  }

  public ngOnInit(): void {

  }

  public submit() {
    if (this.form.valid) {
      this.add(this.form.value);
      this.form.reset();
    }
  }

  public add(post: Post) {
    this.posts = [...this.posts, post];
  }

  public remove(post: Post) {
    this.posts = this.posts.filter(p => p !== post);
  }
}
